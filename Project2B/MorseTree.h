#pragma once

#include "Binary_Search_Tree.h"
#include <fstream>
#include <string>
#include <map>

using namespace std;

// Matt Barchak
// CS 303 Data Structures
// Project 2B
// NOTE: The Binary_Search_tree.h , Binary_Tree.h , and BTNode.h are the binary tree files from canvas
//
//NOTE 2: If necessary: the entire solution is available at: https://gitlab.com/MBarchak/projecttwo






//structure for holding the Morse Information
struct MorseData {
	char letter;
	string code;

	//default constructor
	MorseData() {
		letter = '?';
		code = "dummy value";
	}

	//parameter constructor
	MorseData(char l, string c) {
		letter = l;
		code = c;
	}

	// << overload
	friend ostream& operator<<(ostream& os, const MorseData& data) {
		os << data.letter << " ";
		return os;
	}

	// < operator overload, for the binary search tree comparison
	// < / > below work on the principle that the ' . ' has a lower value than that of the ' _ '
	friend bool operator<(const MorseData left, const MorseData right) {
		int totalSize, leftSize, rightSize;

		leftSize = left.code.size();
		rightSize = right.code.size();

		//the larger string size is iterated against
		if (leftSize > rightSize)
			totalSize = leftSize;
		else
			totalSize = rightSize;

		//iterate through the two strings
		for (int i = 0; i < totalSize; i++) {
			//as long as both aren't at the end of their string
			if (i < leftSize && i < rightSize) {
				//if the left is a . and the right is a _
				if (left.code[i] < right.code[i])
					return true;
				//left = _ , right = .
				else if (left.code[i] > right.code[i])
					return false;
			}
			//if the left string is smaller than the right and the left is done being iterated through
			else if (i > leftSize) {
				//if the right operand has a _
				if (right.code[i] == '_')
					return false;
				//if the right operand has a .
				else
					return true;
			}
			//if the right string is smaller than the left and the right is done being iterated through
			else {
				//if the left operand has a .
				if (left.code[i] == '.')
					return true;
				//if the left operand has a _
				else
					return false;
			}

		}
	}


};

class MorseTree {

private:
	//Morse code file name
	const string FILE = "morse.txt";
	//vector used alongside the map (used to build the tree)
	vector<MorseData> morseVec;

	//vector used for breaking apart a message in the tree decode function
	vector<string> stringSplit;

	//map with a character key, and its pair value is the morse translation
	map<char, string> morseMap;

	//The main Binary Search Tree
	Binary_Search_Tree<MorseData> tree;

	//reads in the file and populates the data structures (except the tree)
	void readMorseFile() {
		//temp storage vars
		char c;
		string s;

		//input file stream
		ifstream stream;
		stream.open(FILE);

		//while the file still has information
		while (!stream.eof()) {
			//read the letter
			stream >> c;
			//read in the morse code
			getline(stream, s);

			//push the data into the vector
			morseVec.push_back(MorseData(c, s));
			//insert the data as a letter (char), code (string) pair into the map
			morseMap.insert(pair<char, string>(c, s));
		}
		//close the file
		stream.close();

	}

	//builds the Morse BST
	void buildMorseTree() {
		//creates a root with a value between . and _
		tree.insert(MorseData('/', "////"));

		//iterates through and grabs the morse codes with a certain number of digits per iteration
		//ex: on the first iteration, i == 1, so all of the morse codes with 1 digit (e and t) are grabbed
		//	  on the second iteration, i ==2, so all of the morse codes with 2 digits (i, a, n , m) are grabbed
		for (int i = 1; i < 5; i++) {
			for (int j = 0; j < morseVec.size(); j++) {
				//if a code has the right size
				if (morseVec[j].code.size() == i) {
					//insert it into the tree
					tree.insert(morseVec[i]);
				}

			}
		}
	}

public:
	//default constructor
	MorseTree() {
		//calls the function that populates the map & vector
		readMorseFile();
		//calls the function to build the tree
		buildMorseTree();
	}


	//takes a string of letters and creates a morse code equivalent through the map
	string encodeMessage(string input) {
		//string to build the return value
		string message = "";

		//iterator for the map
		map<char, string>::iterator itr;

		//loops through the string and finds the matching morse value through the map
		for (int i = 0; i < input.size(); i++) {
			if (input[i] != ' ') {
				message += morseMap.find(input[i])->second;
				message += " ";
			}
		}
		return message;
	}

	//decode function using the map
	string decodeMessage(string input) {

		string message = "";
		//map iterator
		map<char, string>::iterator itr;
		//int representing the index where the substring starts
		int subStrStart = 0;
		//int representing the # of chars to substring
		int charNum = 0;
		for (int i = 0; i < input.size(); i++) {

			//on a delimeter or on the final char of the string
			if (input[i] == ' ' || i == input.size() - 1) {
				//compensating +1 for the final code
				if (i == input.size() - 1)
					charNum = i - subStrStart + 1;
				else
					charNum = i - subStrStart;
				//add the substring code to the vector
				stringSplit.push_back(input.substr(subStrStart, charNum));
				//next substr begins after the space
				subStrStart = i + 1;
			}
			//break out after the last code has been added
			if (i == input.size() - 1)
				break;
		}
		//iterate through the vector of strings and find their map key
		for (int i = 0; i < stringSplit.size(); i++) {
			//iterate through the map for each string code
			for (itr = morseMap.begin(); itr != morseMap.end(); itr++) {
				//if the code matches
				if (stringSplit[i] == itr->second)
					//add the key (letter) to the message
					message += itr->first;
			}
		}

		//return the encoded string
		return message;
	}

	//decodes the message with the BST
	string treeDecode(string input) {
		//string where the message is built
		string message = "";
		//start at the root of the tree
		BTNode<MorseData>* root = tree.getRoot();
		
		//iterate through the given string
		for (int i = 0; i < input.length(); i++) {
			if (input[i] == ' ') {
				//if a full letter has been traversed, get its code and add it to the return string
				message += root->data.letter;
				//returns to the root of the tree after a letter has been found
				root = tree.getRoot();
			}
			// . = go left
			else if (input[i] == '.') {
				//move left down the tree
				root = root->left;
				//if this is the last character of the string
				if (i == input.size() - 1) {
					//add to the message
					message += root->data.letter;
					break;
				}

			}
			// _ = go right
			else {
				//move right down the tree
				root = root->right;
				//if this is the last character of the string
				if (i == input.size() - 1) {
					//add to the message
					message += root->data.letter;
					break;
				}
			}
		}
		//return the decoded string
		return message;
	}


};