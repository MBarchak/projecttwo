// Matt Barchak
// CS 303 Data Structures
// Project 2B
// NOTE: The Binary_Search_tree.h , Binary_Tree.h , and BTNode.h are the binary tree files from canvas
//
//NOTE 2: If necessary: the entire solution is available at: https://gitlab.com/MBarchak/projecttwo




#include "pch.h"
#include <iostream>
#include "MorseTree.h"

int main()
{
	
	MorseTree* t = new MorseTree();
	//t->testing();
	//cout << t->decodeMessage("._ _... _._.");
	string str = t->encodeMessage("abcd");



	system("pause");
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
